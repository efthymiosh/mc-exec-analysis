#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>

#include <set>
#include <map>

#include <stdint.h>
#include <inttypes.h>


#define INIT 0
#define FINI 1
#define VALIDATE 2
#define NEXT_START 3
#define HANDLE_CANDIDATE 4
#define BACKTRACK 5
#define PROSPECT 6

int main(int argc, char *argv[]) {
    unsigned long numblocks_min;
    unsigned long numblocks_max;
    unsigned long numdoms;
    unsigned long percdoms;
    if (argc < 7) {
        printf("Exec: %s NODEFILE DOMINATORFILE SUMEXECFILE NUMBLOCKS_MIN NUMBLOCKS_MAX NUMDOMS PERCENTDOMS\n", argv[0]);
        return 1;
    }

    std::ifstream freqfd, domfd, sumfd;
    freqfd.open(argv[1]);
    domfd.open(argv[2]);
    sumfd.open(argv[3]);
    numblocks_min = atoi(argv[4]);
    numblocks_max = atoi(argv[5]);
    numdoms = atoi(argv[6]);
    percdoms = atoi(argv[7]);

    if (!freqfd.is_open() || !domfd.is_open() || !sumfd.is_open()) {
        printf("Exec: %s NODEFILE DOMINATORFILE SUMEXECFILE NUMBLOCKS_MIN NUMBLOCKS_MAX NUMDOMS PERCENTDOMS\n", argv[0]);
        return 1;
    }

    std::map<unsigned long, unsigned long> nfmap;
    std::map<unsigned long, std::set<unsigned long> *> domap;

    std::map<unsigned long, unsigned long> benefitMap;
    std::map<unsigned long, unsigned long> costMap;
    std::multimap<unsigned long, unsigned long> invBenefitMap;

    unsigned long node;
    unsigned long domNode;
    unsigned long amt;

    while (freqfd >> node) {
        char ch;
        freqfd.get(ch);
        freqfd >> amt;
        if (freqfd.fail()) {
            std::cout << "Invalid input" << std::endl;
            return 0;
        }
        nfmap[node] = amt;
    }

    unsigned long exec;
    sumfd >> exec;

    while (domfd >> domNode) {
        char ch;
        domfd.get(ch);
        domfd >> node;
        if (domfd.fail()) {
            std::cout << "Invalid input" << std::endl;
            return 0;
        }
        if (domap[domNode] == nullptr)
            domap[domNode] = new std::set<unsigned long>();
        domap[domNode]->insert(node);
        benefitMap[domNode] += nfmap[node];
    }

    for (std::pair<unsigned long, std::set<unsigned long> *> domPair : domap)
        costMap[domPair.first] = domPair.second->size();

    for (std::pair<unsigned long, unsigned long> p : benefitMap) {
        if (costMap[p.first] <= numblocks_max)
            invBenefitMap.insert(std::pair<unsigned long, unsigned long>(p.second, p.first));
    }

    size_t invBenMapSize = invBenefitMap.size();

    int thres = invBenMapSize * percdoms / 100;
    int i = 0;
    auto cutter = invBenefitMap.rbegin();
    for (; cutter != invBenefitMap.rend(); ++cutter) {
        if (++i > thres)
            break;
    }

    std::multimap<unsigned long, unsigned long>::reverse_iterator mmit;

    double total = 0.0;


    std::multimap<unsigned long, unsigned long>::reverse_iterator *itArr;
    unsigned long tupleBenefit, tupleCost, maxBenefit;
    long position;
    int iter = 0;
    bool flag = false;
    int state = INIT;
    unsigned long *prospectArr;
    char *disjointPairs;
    printf("%11s %11s %s\n", "Benefit", "~Exec Time", "Tuple");
    while(state != FINI) {
SWITCHLAB: switch (state) {
            case INIT: /* Initialize and drop to validation */
                itArr = new std::multimap<unsigned long, unsigned long>::reverse_iterator[numdoms];
                itArr[0] = invBenefitMap.rbegin();
                prospectArr = (unsigned long *) calloc(invBenMapSize * numdoms, sizeof(unsigned long));
                disjointPairs = (char *) calloc(invBenMapSize * invBenMapSize, sizeof(char));
                position = 0;
                tupleBenefit = 0;
                tupleCost = 0;
                maxBenefit = 0;

                state = VALIDATE;
            case VALIDATE: /* Validate current dominator at position. */
                if (itArr[position] == cutter) {
                    state = BACKTRACK;
                    continue;
                }

                //cost check
                if (tupleCost + costMap[itArr[position]->second] > numblocks_max - (numdoms - position + 1)) {
                    itArr[position]++;
                    continue;
                }

                /**
                 * Use Dynamic Programming to avoid recalculating disjoint pairs of nodes.
                 * Half the array is not used, since the pairs stored and loaded, (A,B), have
                 * the property: A > B.
                 */
                for (unsigned long pos = 0; pos < position; pos++) {
                    char res = disjointPairs[itArr[pos]->second * invBenMapSize + itArr[position]->second];
                    switch (res) {
                        case 0: //not calculated; calculate
                            for (unsigned long node : *domap[itArr[position]->second])
                                if (domap[itArr[pos]->second]->count(node) != 0) {
                                    disjointPairs[itArr[pos]->second * invBenMapSize + itArr[position]->second] = 2;
                                    itArr[position]++;
                                    goto SWITCHLAB;
                                }
                            disjointPairs[itArr[pos]->second * invBenMapSize + itArr[position]->second] = 1;
                        case 1: //disjoint; continue to next
                            continue;
                        case 2: //overlapping;
                            itArr[position]++;
                            goto SWITCHLAB;

                    }
                }

                tupleBenefit += benefitMap[itArr[position]->second];
                tupleCost += costMap[itArr[position]->second];

                ++position;
                if (position == numdoms) {
                    state = HANDLE_CANDIDATE;
                    continue;
                }
                itArr[position] = itArr[position - 1];
                itArr[position]++;

                state = PROSPECT;
                break;
            case HANDLE_CANDIDATE:
                if ((tupleCost >= numblocks_min)
                        && (tupleCost <= numblocks_max)
                        && (tupleBenefit > maxBenefit)) {

                    maxBenefit = tupleBenefit;

                    double cur_time = ((double)tupleBenefit) / exec * 100;
                    printf("%11lu%11.2f%% [", tupleBenefit, cur_time);
                    for (int i = 0; i < numdoms; ++i)
                        printf(" %" PRIu64, itArr[i]->second);
                    printf("]\n");
                }

                state = BACKTRACK;
                break;
            case BACKTRACK: /* Need to backtrack, no available solutions */
                --position;
                if (position < 0) {
                    state = FINI;
                    continue;
                }

                tupleBenefit -= benefitMap[itArr[position]->second];
                tupleCost -= costMap[itArr[position]->second];

                itArr[position]++;
                state = PROSPECT;
                break;
            case PROSPECT:
                /**
                 * When having reached a turn point for the search, attempt to prune
                 * search space by calculating the possible best candidate from this point
                 * on and comparing to previous tests. Each time the test fails,
                 * move to the previous tuple-position and repeat for n+1 items.
                 *
                 * The possible best candidate at search point Xn is the tuple
                 * with (X1, .. , Xn, Xn->nextBy(1), Xn->nextBy(2), .., Xn->nextBy(K) )
                 */

                unsigned long prospectiveBenefit = tupleBenefit;
                long pos = position;

                /* Assume immediate next candidate tuple consists of Disjoint Dominators.*/
                prospectiveBenefit += prospectArr[itArr[position]->second * numdoms + position];
                if (prospectiveBenefit == tupleBenefit) {
                    unsigned long diff = 0;
                    auto itPos = itArr[position];
                    for (++itPos ; (pos < numdoms) && (itPos != cutter); ++pos, ++itPos)
                        diff += benefitMap[itPos->second];
                    if (itPos == cutter) {
                        state = BACKTRACK;
                        continue;
                    }
                    prospectArr[itArr[position]->second * numdoms + position] = diff;
                    prospectiveBenefit += diff;
                }
                if (prospectiveBenefit < maxBenefit) {
                    /* itPos == cutter: Failed to create best candidate tuple, out of dominators.
                     * prospectiveBenefit < maxBenefit: No point continuing. Nothing from here on will beat maxBenefit.
                     * Reset previous in these cases. */
                    state = BACKTRACK;
                    continue;
                }

                state = VALIDATE;
                break;
           }
    }
    return 0;
}
